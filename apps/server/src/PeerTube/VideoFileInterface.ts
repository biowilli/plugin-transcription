export interface VideoFileInterface {
  path: string;
  url: string;
  resolution: number;
  size: number;
  fps: number;
}
