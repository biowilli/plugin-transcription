import { LanguageModelRepository } from './LanguageModelRepository';
import { PluginStorageManager } from '@peertube/peertube-types';
import { availableModels } from '../__mocks__/models';
import { VoskModelPageParser } from '../Recognizer';

describe('LanguageModelRepository', () => {
  test('parse Vosk models page', async () => {
    const mockedStorage: PluginStorageManager = {
      storeData: jest.fn(() => Promise.resolve()),
      getData: jest.fn(),
    };
    const voskModelPageParser = new VoskModelPageParser(
      'https://alphacephei.com/vosk/models/model-list.json',
      console,
      ['fr', 'en']
    );
    const languageModelRepository = new LanguageModelRepository(mockedStorage, voskModelPageParser, console);
    await languageModelRepository.store(availableModels);
    expect(languageModelRepository.getModels()).toEqual(availableModels);

    // @ts-ignore
    const { type } = languageModelRepository.findSmallestModelForLanguage('fr');
    expect(type).toBe('small');
  });
});
