import { LanguageModelRepository } from '../LanguageModelRepository';
import { pluginStorageManager } from '../../__mocks__/PeerTube/PluginStorageManager';
import { availableModels } from '../../__mocks__/models';

// @ts-ignore
export const languageModelRepository: LanguageModelRepository = {
  models: [],
  storage: pluginStorageManager,
  getModelsByLanguage: jest.fn(() => availableModels),
  addModel: jest.fn(),
  getModels: jest.fn(() => availableModels),
  load: jest.fn(),
  store: jest.fn(),
  findSmallestModelForLanguage: jest.fn(),
};
