export type TerminationSignal = 'SIGTERM' | 'SIGINT';

export interface TerminationRequestInterface {
  signal: TerminationSignal;
}

export class TerminationRequest implements TerminationRequestInterface {
  public signal: TerminationSignal;
  constructor(signal: TerminationSignal) {
    this.signal = signal;
  }
}

export function isTerminationRequest(obj: any): obj is TerminationRequest {
  return obj.signal !== undefined;
}
