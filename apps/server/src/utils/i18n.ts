export const LOCALE_REGEXP = new RegExp(/([a-z]{2})-[a-z]{2}/i);

export function extractLanguage(languageLike: string) {
  const localeMatches = languageLike.match(LOCALE_REGEXP);
  if (localeMatches) {
    return localeMatches[1];
  }
  return languageLike;
}
