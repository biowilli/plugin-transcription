import { get } from './request';
import { jest } from '@jest/globals';

jest.setTimeout(20000);

describe('request', () => {
  test('get', async () => {
    await get(`http://localhost:${process.env.PORT || 9001}/`);
  });
});
